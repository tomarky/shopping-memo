// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyRecord.java  レコードストア処理
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
//import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import java.util.*;
import javax.microedition.rms.*;
import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyRecord {

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	public MyRecord() {
	}

	//TextField1sのデータを保存できる容量が残ってるかチェックする
	public static boolean checkSave(String recordname, TextField1s tf1s) {
		byte[] b=tf1s.toBytes();
		
		if (b==null)
			return false;
		
		int sz=b.length;
		boolean ch=false;
		
		RecordStore rs=null;
		try {
			rs=RecordStore.openRecordStore(recordname,false);
			int sa=rs.getSizeAvailable();
			if (sa>sz+300) {
				ch=true;
			}
		} catch (RecordStoreNotFoundException re) {
			System.out.println("MyRecord.chackSave::"+re.toString());
			ch=true;
		} catch (Exception e) {
			System.out.println("MyRecord.chackSave::"+e.toString());
		}

		if (rs!=null) {
			try {
				rs.closeRecordStore();
			} catch (Exception e) {
				System.out.println("MyRecord.chackSave(close)::"+e.toString());
			}
		}
		
		return ch;
	}

	//TextField1sのデータを保存する
	public static boolean saveData(String recordname, String name, TextField1s tf1s) {
		byte[] b=tf1s.toBytes();
		
		if (b==null) return false;

		int sz=b.length;

		long t=System.currentTimeMillis();

		byte[] bname=name.getBytes();
		int n=bname.length;
		
		tf1s.name=name;
		tf1s.datetime=t;
		
		byte[] bdata=null;
		
		//書き込むデータの生成
		ByteArrayOutputStream out=new ByteArrayOutputStream(4);
		DataOutputStream dos=new DataOutputStream(out);
		try {
			dos.writeLong(t);
			dos.writeInt(n);
			dos.write(bname,0,n);
			dos.writeInt(tf1s.goodscount);
			dos.write(b,0,sz);
			bdata=out.toByteArray();
			dos.close();
		} catch (Exception e) {
			System.out.println("MyRecord.saveData(write)::"+e.toString());
			try {
				dos.close();
			} catch (Exception e2) {
				System.out.println("MyRecord.saveData(dos.close)::"+e2.toString());
			}
			return false;
		}
		
		//レコードストアへの書き込み
		RecordStore rs=null;
		
		try {
			rs=RecordStore.openRecordStore(recordname,true);
			rs.addRecord(bdata,0,bdata.length);
			rs.closeRecordStore();
		} catch (Exception e) {
			System.out.println("MyRecord.saveData(RecordStore)::"+e.toString());
			if (rs!=null) {
				try {
					rs.closeRecordStore();
				} catch (Exception e2) {
					System.out.println("MyRecord.saveData(rs.close)::"+e2.toString());
				}
			}
			return false;
		}
		return true;
	}
	
	//ロードできるデータのリストを作成する
	public static boolean loadList(String recordname, TextField2s tf2s) {
		tf2s.reset();
		
		ByteArrayInputStream in=null;
		DataInputStream      dis=null;
		RecordStore          rs=null;
		RecordEnumeration    re=null;
		MyData               md=null;
		byte[]               b=null,bname=null;
		int                  rid=0,nrec=0,i,n;
		long                 t=0L;
		
		try {
			rs=RecordStore.openRecordStore(recordname,false);
			re=rs.enumerateRecords(null,null,false);
			
			nrec=rs.getNumRecords();
			for (i=0;i<nrec;i++) {
				rid=re.nextRecordId();
				b=rs.getRecord(rid);
				in=new ByteArrayInputStream(b);
				dis=new DataInputStream(in);
				t=dis.readLong();
				n=dis.readInt();
				bname=new byte[n];
				dis.read(bname);
				md=new MyData(new String(bname),rid,t);
				tf2s.add(new TextField2(md));
				dis.close();
				b=null;
			}
			re.destroy();
			rs.closeRecordStore();
			return true;
		} catch (Exception e) {
			System.out.println("MyRecord.loadList::"+e.toString());
			if (dis!=null) {
				try {
					dis.close();
				} catch (Exception e2) {
					System.out.println("MyRecord.loadList(dis.close)::"+e2.toString());
				}
			}
			if (re!=null) {
				try {
					re.destroy();
				} catch (Exception e2) {
					System.out.println("MyRecord.loadList(re.destroy)::"+e2.toString());
				}
			}
			if (rs!=null) {
				try {
					rs.closeRecordStore();
				} catch (Exception e2) {
					System.out.println("MyRecord.loadList(rs.close)::"+e2.toString());
				}
			}
			return false;
		}
	}

	public static boolean loadData(String recordname, int recordId, TextField1s tf1s) {
		
		RecordStore          rs=null;
		byte[]               bData=null;

		try {
			rs=RecordStore.openRecordStore(recordname,false);
			bData=rs.getRecord(recordId);
			rs.closeRecordStore();
		} catch (Exception e) {
			System.out.println("MyRecord.loadData(RecordStore)::"+e.toString());
			if (rs!=null) {
				try {
					rs.closeRecordStore();
				} catch (Exception e2) {
					System.out.println("MyRecord.loadData(rs.close)::"+e2.toString());
				}
			}
			return false;
		}

		ByteArrayInputStream in=new ByteArrayInputStream(bData);
		DataInputStream      dis=new DataInputStream(in);
		MyGoods              mg=null;
		byte[]               b=null;
		int                  n,i,sz;

		tf1s.reset();

		try {
			tf1s.datetime=dis.readLong();   //DateTime
			sz=dis.readInt();  //MemoName Length
			b=new byte[sz];   
			dis.read(b);      //MemoName
			tf1s.name=new String(b);
			tf1s.goodscount=dis.readInt();    //DataSize
			
			n=dis.readInt();  //Count
			for (i=0;i<n;i++) {
				sz=dis.readInt();
				b=new byte[sz];
				dis.read(b);
				tf1s.add(new MyGoods(b));
			}
			dis.close();
		} catch (Exception e) {
			System.out.println("MyRecord.loadData(DataInputStream)::"+e.toString());
			try {
				dis.close();
			} catch (Exception e2) {
				System.out.println("MyRecord.loadData(dis.close)::"+e.toString());
			}
			return false;
		}
		return true;
	}
	
	
	public static boolean deleteData(String recordname, int recordId) {
		RecordStore rs=null;
		try {
			rs=RecordStore.openRecordStore(recordname,false);
			rs.deleteRecord(recordId);
			rs.closeRecordStore();
		} catch (Exception e) {
			System.out.println("MyRecord.deleteData(RecordStore)::"+e.toString());
			if (rs!=null) {
				try {
					rs.closeRecordStore();
				} catch (Exception e2) {
					System.out.println("MyRecord.deleteData(rs.close)::"+e.toString());
				}
			}
			return false;
		}
		return true;
	}
	

	//データ(int型)の書き込み【追加】
	public static void saveInt(String recordname, int value) {
		
		//データをバイトデータに変換
		ByteArrayOutputStream b=new ByteArrayOutputStream(4);
		DataOutputStream d=new DataOutputStream(b);
		try { d.writeInt(value);} catch (Exception e) {}
		try { d.flush();} catch (Exception e) {}
		byte[] w=b.toByteArray();
		try { d.close();} catch (Exception e) {}
		try { b.close();} catch (Exception e) {}
		
		RecordStore rs=null;
		try {
			//レコードストアの接続
			rs=RecordStore.openRecordStore(recordname, true);

			//レコードの追加
			if (rs.getNumRecords()==0) {
				rs.addRecord(w,0,w.length);
			}
			//レコードの更新
			else {
				rs.setRecord(1,w,0,w.length);
			}
			//レコードストアの切断
			rs.closeRecordStore();
			
		} catch (Exception e) {
			System.out.println("MyRecord.saveInt::"+e.toString());
			//例外処理
			try {
				if (rs!=null) {rs.closeRecordStore();}
			} catch (Exception e2) {
				System.out.println("MyRecord.saveInt(close)::"+e2.toString());
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//データ(int型)の読み込み【追加】
	public static int loadInt(String recordname, int defaultvalue) {

		RecordStore rs=null;		
		try {
			//レコードストアの接続
			rs=RecordStore.openRecordStore(recordname,false);

			//レコードの読み込み
			byte[] w=rs.getRecord(1);

			//レコードストアの切断
			rs.closeRecordStore();
			
			//バイトデータをハイスコアに変換
			ByteArrayInputStream b=new ByteArrayInputStream(w);
			DataInputStream d=new DataInputStream(b);
			int value;
			
			try {
				value=d.readInt();
			} catch (Exception e3) {
				value=defaultvalue;
			}
			try { d.close();} catch (Exception e3) {}
			try { b.close();} catch (Exception e3) {}

			return(value);

		} catch (Exception e) {
			try {
			System.out.println("MyRecord.loadInt::"+e.toString());
			//例外処理
				if (rs!=null) { rs.closeRecordStore();}
			} catch (Exception e2) {
				System.out.println("MyRecord.loadInt(close)::"+e2.toString());
			}
			return (defaultvalue);
		}
	}

} //MyRecordの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
