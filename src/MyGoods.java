// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyGoods.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
//import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
import java.io.*;
//import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyGoods {

	public  String    name=null;
	public  long      price=0L;
	public  long      count=0L;
	public  boolean   intax=false;

	//コンストラクタ
	MyGoods(String n, long p, long c, boolean i) {
		name=new String(n);
		price=p;
		count=c;
		intax=i;
	}
	MyGoods(byte[] bData) {
		ByteArrayInputStream in=new ByteArrayInputStream(bData);
		DataInputStream      dis=new DataInputStream(in);
		
		try {
			int    n=dis.readInt();
			byte[] b=new byte[n];
			dis.read(b);
			name=new String(b);
			price=dis.readLong();
			count=dis.readLong();
			intax=dis.readBoolean();
			dis.close();
		} catch (Exception e) {
			System.out.println("MyGoods(DataInputStream)::"+e.toString());
			try {
				dis.close();
			} catch (Exception e2) {
				System.out.println("MyGoods(dis.close)::"+e.toString());
			}
		}
	}
	
	//詳細情報を返す
	public String details() {
		String s=null;
		String s2="";
		
		if (intax)
			s2=" 込";
		
		s=Long.toString(price)+" x "
		          +Long.toString(count)+" ( "
		          +Long.toString(total())+" )";
		return s+s2;
	}
	
	//単純に金額x個数の計算値を返す
	public long total() {
		return price*count;
	}
	
	//税込み額の総額を返す
	public long total(int tax){
		if (tax==-100) return 0L;
		if (intax)
			return price*count;
		double d=(double)(price*count);
		d*=((double)(100+tax))/100.0;
		return (long)d;
	}
	
	//１個当たりの税込み額を返す
	public long pricewithtax(int tax){
		if (tax==-100) return 0L;
		if (intax)
			return price;
		double d=(double)price;
		d*=((double)(100+tax))/100.0;
		return (long)d;
	}
	
	//税抜き額の総額を返す
	public long totalnotax(int tax) {
		if (tax==-100) return 0L;
		if (intax) {
			double d=(double)price;
			d*=100.0/((double)(100+tax));
			d=Math.ceil(d);
			return count*(long)d;
		} else {
			return price*count;
		}
	}

	//１個当たりの税抜き額を返す
	public long pricenotax(int tax) {
		if (tax==-100) return 0L;
		if (intax) {
			double d=(double)price;
			d*=100.0/((double)(100+tax));
			return (long)d;
		} else {
			return price;
		}
	}
	
	//消費税額の総額を返す
	public long totaltax(int tax) {
		if (tax==-100) return 0L;
		double d;
		if (intax) {
			d=(double)price;
			d*=((double)tax)/((double)(100+tax));
			return count*((long)d);
		} else {
			d=(double)(price*count);
			d*=((double)tax)/100.0;
			return (long)d;
		}
	}
	
	//１個あたりの消費税額を返す
	public long thistax(int tax) {
		if (tax==-100) return 0L;
		double d=(double)price;
		if (intax) {
			d*=(double)tax/(100.0+(double)tax);
		} else {
			d*=(double)tax/100.0;
		}
		return (long)d;
	}

	//保存用にbyte配列に変換
	public byte[] toBytes() {
		ByteArrayOutputStream out=new ByteArrayOutputStream(4);
		DataOutputStream dos=new DataOutputStream(out);

		byte[] b=name.getBytes();
		
		int n=b.length;
		
		try {
			dos.writeInt(n);
			dos.write(b,0,n);
			dos.writeLong(price);
			dos.writeLong(count);
			dos.writeBoolean(intax);
			b=out.toByteArray();
		} catch (Exception e) {
			System.out.println("MyGoods.toBytes(write)::"+e.toString());
			b=null;
		}
		
		try {
			dos.close();
		} catch (Exception e) {
			System.out.println("MyGoods.toBytes(close)::"+e.toString());
		}
		
		return b;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyGoodsの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
