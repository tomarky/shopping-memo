// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form8.java  フォーム
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form8 extends Form {
	
	public static final int   ERR_SAVE=0,
	                          ERR_DOSAVE=1,
	                          ERR_LOAD=2,
	                          ERR_OPEN=3,
	                          ERR_NODATA=4,
	                          SUC_SAVE=100,
	                          SUC_LOAD=101;

	public static String formtitle(int type) {
		switch (type) {
		case ERR_SAVE:
		case ERR_DOSAVE:
		case ERR_LOAD:
		case ERR_OPEN:
		case ERR_NODATA:
			return "エラー";
		case SUC_SAVE:
		case SUC_LOAD:
			return "成功";
		default:
			return "メッセージ";
		}
	}

	//コンストラクタ
	Form8(CommandListener cl, int type) {
		super(Form8.formtitle(type));
		
		StringItem si=null;
		
		String title=null,mes=null;
		
		switch (type) {
		case ERR_DOSAVE:
			title="保存に失敗しました";
			break;
		case ERR_SAVE:
			title="保存に失敗しました";
			mes="\n\n原因として保存容量不足が考えられます\n"
			    +"必要ないメモを削除する等して\n"
			    +"十分な保存容量を確保してください";
			break;
		case ERR_LOAD:
			title="データの読み込みに失敗しました";
			break;
		case ERR_OPEN:
			title="保存されているメモがありません";
			break;
		case ERR_NODATA:
			title="メモにデータがありません";
			break;
		case SUC_SAVE:
			title="保存に成功しました";
			break;
		case SUC_LOAD:
			title="データの読み込みに成功しました";
			break;
		}
		append(new StringItem(title,mes));
		
		addCommand(new Command("OK",Command.OK,0));
		setCommandListener(cl);
	}
	Form8(CommandListener cl, String ftitle, String stitle, String mes) {
		super(ftitle);
		
		append(new StringItem(stitle,mes));
		
		addCommand(new Command("OK",Command.OK,0));
		setCommandListener(cl);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form8の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
