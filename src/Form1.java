// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form1.java  フォーム
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form1 extends Form implements CommandListener, ItemCommandListener {
	
	private final String RC_TAX   ="tm.shopping.tax",
	                     RC_RECNUM="tm.shopping.recnum",
	                     RC_DATA  ="tm.shopping.data";
                 

	private Command      cmds[]=new Command[7];

	private Form2        form2=null; //追加フォーム
	private Form3        form3=null; //終了確認フォーム
	private Form4        form4=null; //税率設定フォーム
	private Form5        form5=null; //計算結果フォーム
	private Form6        form6=null; //削除確認フォーム
	private Form7        form7=null; //保存フォーム
	private Form8        form8=null; //メッセージフォーム
	private Form9        form9=null;
	private Form10       form10=null;
	private Form11       form11=null;
	private Form12       form12=null;
	private Form13       form13=null;
	
	private MIDlet       midlet;

	private TextField1s  tf1s=new TextField1s(this);
	private TextField2s  tf2s=new TextField2s(this);

	private int          recordNum=0;
	private int          goodscount=0;
	private int          tax=0;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	Form1(MIDlet m) {
		super("買い物メモ");
		
		midlet=m;
		
		cmds[0]=new Command("追加",Command.SCREEN,0);
		cmds[1]=new Command("計算",Command.SCREEN,1);
		cmds[2]=new Command("終了",Command.EXIT  ,0);
		cmds[3]=new Command("設定",Command.SCREEN,2);
		cmds[4]=new Command("保存",Command.SCREEN,3);
		cmds[5]=new Command("開く",Command.SCREEN,4);
		cmds[6]=new Command("ﾘｾｯﾄ",Command.SCREEN,5);
		
		int i;
		for (i=0;i<cmds.length;i++)
			addCommand(cmds[i]);
		
		setCommandListener(this);
		
		tax=MyRecord.loadInt(RC_TAX,5);
		recordNum=MyRecord.loadInt(RC_RECNUM,0);
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void setDisplay(Displayable d) {
		Display.getDisplay(midlet).setCurrent(d);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c, Displayable d) {
		TextField1 tf1=null;
		TextField2 tf2=null;
		MyGoods    mg=null;
		String     name=null;
		int        num,cnt,j;
		
		//追加フォーム
		if(d==form2) {
			if (c.getCommandType()==Command.OK) {
				mg=form2.data();
				switch (form2.state) {
				case 0:
					goodscount++;
					tf1=new TextField1(this,mg);
					insert(0,tf1);
					tf1s.add(tf1);
					break;
				case 1:
					tf1=tf1s.getTF1(mg);
					tf1.update();
					break;
				}
			}
			setDisplay(this);
			form2=null;
			return;
		}
		
		//終了フォーム
		if (d==form3) {
			if (c.getCommandType()==Command.OK) {
				midlet.notifyDestroyed();
			} else {
				setDisplay(this);
				form3=null;
			}
			return;
		}

		//税率設定フォーム
		if (d==form4) {
			if (c.getCommandType()==Command.OK) {
				tax=form4.data();
				MyRecord.saveInt(RC_TAX,tax);
			}
			setDisplay(this);
			form4=null;
			return;
		}
		
		//計算フォーム
		if (d==form5) {
			if (c.getCommandType()==Command.OK) {
				setDisplay(this);
				form5=null;
				return;
			}
			form13=new Form13(this,tf1s.text(tax));
			setDisplay(form13);
			return;
		}
		
		//削除フォーム
		if (d==form6) {
			if (c.getCommandType()==Command.OK) {
				tf1=form6.data();
				num=tf1s.number(tf1);
				delete(num);
				tf1s.del(tf1);
				tf1.mygoods=null;
			}
			setDisplay(this);
			form6=null;
			return;
		}
		
		//保存フォーム
		if (d==form7) {
			if (c.getCommandType()==Command.OK) {
				name=form7.data();
				tf1s.goodscount=goodscount;
				if (MyRecord.saveData(RC_DATA,name,tf1s)) {
					form8=new Form8(this,Form8.SUC_SAVE);
					recordNum++;
					MyRecord.saveInt(RC_RECNUM,recordNum);
				} else {
					form8=new Form8(this,Form8.ERR_DOSAVE);
				}
				setDisplay(form8);
				form7=null;
				return;
			}
			setDisplay(this);
			form7=null;
			return;
		}
		
		//メッセージフォーム
		if (d==form8) {
			setDisplay(this);
			form8=null;
		}
		
		//開くフォーム
		if (d==form9) {
			setDisplay(this);
			form9=null;
			tf2s.reset();
		}
		
		//リセットフォーム
		if (d==form10) {
			if (c.getCommandType()==Command.OK) {
				deleteAll();
				tf1s.reset();
				goodscount=0;
			}
			setDisplay(this);
			form10=null;
		}
		
		//レコード削除確認フォーム
		if (d==form11) {
			if (c.getCommandType()==Command.OK) {
				num=form11.num;
				tf2=form11.tf2;
				MyRecord.deleteData(RC_DATA,tf2.mydata.recordId);
				form9.del(num);
				tf2s.del(tf2);
			}
			setDisplay(form9);
			form11.tf2=null;
			form11=null;
		}
		
		//開く確認フォーム
		if (d==form12) {
			if (c.getCommandType()==Command.OK) {
				tf2=form12.tf2;
				deleteAll();
				tf1s.reset();
				if (MyRecord.loadData(RC_DATA,tf2.mydata.recordId,tf1s)) {
					cnt=tf1s.count();
					for (j=0;j<cnt;j++)
						append(tf1s.getCnt(j));
					goodscount=tf1s.goodscount;
					form8=new Form8(this,Form8.SUC_LOAD);
				} else {
					tf1s.reset();
					form8=new Form8(this,Form8.ERR_LOAD);
				}
				setDisplay(form8);
				form9=null;
				form12=null;
				return;
			}
			setDisplay(form9);
			form12=null;
			return;
		}
		
		if (d==form13) {
			setDisplay(form5);
			form13=null;
		}
		
		//このフォーム
		if (c==cmds[0]) {         //追加
			form2=new Form2(this,goodscount+1);
			setDisplay(form2);

		} else if (c==cmds[1]) {  //計算
			form5=new Form5(this,tf1s,tax);
			setDisplay(form5);

		} else if (c==cmds[2]) {  //終了
			form3=new Form3(this);
			setDisplay(form3);

		} else if (c==cmds[3]) {  //設定
			form4=new Form4(this,tax);
			setDisplay(form4);

		} else if (c==cmds[4]) {  //保存
			cnt=tf1s.count();
			if (cnt>0) {
				if (MyRecord.checkSave(RC_DATA,tf1s)) {
					form7=new Form7(this,recordNum+1);
					setDisplay(form7);
				} else {
					form8=new Form8(this,Form8.ERR_SAVE);
					setDisplay(form8);
				}
			} else {
				form8=new Form8(this,Form8.ERR_NODATA);
				setDisplay(form8);
			}
			
		} else if (c==cmds[5]) {  //開く
			if (MyRecord.loadList(RC_DATA,tf2s)) {
				form9=new Form9(this,tf2s);
				setDisplay(form9);
			} else {
				form8=new Form8(this,Form8.ERR_OPEN);
				setDisplay(form8);
			}
		} else if (c==cmds[6]) {  //ﾘｾｯﾄ
			form10=new Form10(this);
			setDisplay(form10);
		}

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アイテムコマンドイベント
	public void commandAction(Command c, Item item) {
		TextField1 tf1;
		TextField2 tf2;
		int n,i,p,cnt,j;
		
		n=tf1s.count();
		for (i=0;i<n;i++) {
			tf1=tf1s.getCnt(i);
			if (tf1==item) {
				p=c.getPriority();
				switch (p) {
				case TextField1.CMD_CHANGE:
					form2=new Form2(this,tf1.mygoods);
					setDisplay(form2);
					break;
				case TextField1.CMD_DELETE:
					form6=new Form6(this,tf1);
					setDisplay(form6);
					break;
				}
				return;
			}
		}
		
		n=tf2s.count();
		for (i=0;i<n;i++) {
			tf2=tf2s.getCnt(i);
			if (tf2==item) {
				p=c.getPriority();
				switch (p) {
				case TextField2.CMD_OPEN:
					form12=new Form12(this,tf2);
					setDisplay(form12);
					break;
				case TextField2.CMD_DELETE:
					form11=new Form11(this,i,tf2);
					setDisplay(form11);
//					MyRecord.deleteData(RC_DATA,tf2.mydata.recordId);
//					form9.del(i);
//					tf2s.del(tf2);
					break;
				}
				return;
			}
		}
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
