// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form9.java  フォーム
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form9 extends Form {
	
	TextField2s tf2s=null;
	
	//コンストラクタ
	Form9(CommandListener cl, TextField2s t) {
		super("開く");
		
		tf2s=t;
		
		int n=tf2s.count();
		int i;
		
		for (i=0;i<n;i++)
			append(tf2s.getCnt(i));
			
		addCommand(new Command("戻る",Command.SCREEN,0)); 
		
		setCommandListener(cl);
	}
	
	public void del(int n) {
		delete(n);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form9の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
