// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form2.java  フォーム
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form2 extends Form {
	
	private Command      cmds[]=new Command[2];

	private TextField    tfName;
	private TextField    tfPrice;
	private TextField    tfCount;
	private ChoiceGroup  cgInTax;
	
	private int          iNum=0;
	
	private MyGoods      mygoods=null;
	
	public int           state=0;

	private void init(CommandListener cl) {
	
		String[] s={"税込み価格","税抜き価格"};
		
		tfName =new TextField("商品名",mygoods.name,50,TextField.ANY);
		tfPrice=new TextField("値段",Long.toString(mygoods.price),6,TextField.NUMERIC);
		tfCount=new TextField("個数",Long.toString(mygoods.count),4,TextField.NUMERIC);
		cgInTax=new ChoiceGroup("消費税",Choice.EXCLUSIVE,s,null);
		if (mygoods.intax) 
			cgInTax.setSelectedIndex(0,true);
		else
			cgInTax.setSelectedIndex(1,true);

		append(tfName);
		append(tfPrice);
		append(tfCount);
		append(cgInTax);
		
		cmds[0]=new Command("OK"    ,Command.OK    ,0);
		cmds[1]=new Command("Cancel",Command.CANCEL,1);
		
		int i;
		for (i=0;i<cmds.length;i++)
			addCommand(cmds[i]);
		
		setCommandListener(cl);
	}

	//コンストラクタ
	Form2(CommandListener cl, int num) {
		super("追加");
		
		iNum=num;
		
		mygoods=new MyGoods("Goods#"+Integer.toString(num),0,1,true);
		
		state=0;
		
		init(cl);
	}
	Form2(CommandListener cl, MyGoods mg) {
		super("変更");
		
		mygoods=mg;
		
		state=1;
		
		init(cl);
	}
	
	public MyGoods data() {
	
		if (tfName.size()==0)  tfName.setString("Goods#"+Integer.toString(iNum));
		if (tfPrice.size()==0) tfPrice.setString("0");
		if (tfCount.size()==0) tfCount.setString("1");
		
		long p=Long.parseLong(tfPrice.getString());
		long c=Long.parseLong(tfCount.getString());
		
		if (p<0) p=0L;
		if (c<0) c=0L;
		
		boolean b=(cgInTax.getSelectedIndex()==0);
		
		mygoods.name=tfName.getString();
		mygoods.price=p;
		mygoods.count=c;
		mygoods.intax=b;
	
		return mygoods;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form2の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
