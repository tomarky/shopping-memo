// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// TextField1s.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
import java.io.*;
import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class TextField1s {

	private ItemCommandListener icl=null;
	
	private MyList mylist=null;
	
	public String name=null;
	public long   datetime=0L;
	public int    goodscount=0;

	//コンストラクタ
	TextField1s(ItemCommandListener c) {
		icl=c;
	}
	
	public String text(int tax) {
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		PrintStream ps=new PrintStream(out);
		MyList list=null;
		MyGoods mg=null;
		long total=0L;
		
		if (name!=null) {
			ps.println(name);
			ps.println((new Date(datetime)).toString());
			ps.println();
		} else {
			ps.println((new Date(System.currentTimeMillis())).toString());
			ps.println();
		}
		
		list=mylist;
		while (list!=null) {
			mg=list.textfield1.mygoods;
			ps.println(mg.name);
			ps.println(mg.details());
			ps.println();
			total+=mg.total(tax);
			list=list.next;
		}
		
		ps.println("------------------------");
		ps.println("( 消費税率 "+Integer.toString(tax)+" % )");
		ps.println("合計 "+Long.toString(total));
		
		String s=new String(out.toString());
		
		ps.close();
		
		return s;
	}

	public void reset() {
		if (mylist!=null) {
			mylist.reset();
			mylist=null;
		}
		name=null;
		datetime=0L;
		goodscount=0;
	}

	//渡したTextField1を加える
	public void add(TextField1 tf1) {
		mylist=new MyList(tf1,mylist);
	}
	
	public void add(MyGoods mg) {
		add(new TextField1(icl,mg));
	}

	//渡したTextField1がリストに存在するなら削除する
	public void del(TextField1 tf1) {
		if (mylist!=null)
			mylist=mylist.del(tf1);
	}

	//渡したMyGoodsを持つTextField1があったら返す
	public TextField1 getTF1(MyGoods mg) {
		if (mylist!=null)
			return mylist.getTF1(mg);
		return null;
	}

	//リストのcnt番目TextField1を返す
	public TextField1 getCnt(int cnt) {
		if (mylist!=null)
			return mylist.getCnt(cnt);
		return null;
	}

	//リストに登録されてる総数を返す
	public int count() {
		if (mylist!=null)
			return mylist.count();
		return 0;
	}

	//渡したTextField1が何番目かを返す
	public int number(TextField1 tf1) {
		if (mylist!=null)
			return mylist.number(tf1);
		return -1;
	}

	//保存用のbyte配列にしてデータを返す
	public byte[] toBytes() {
		if (mylist!=null) {
			ByteArrayOutputStream out=new ByteArrayOutputStream(4);
			DataOutputStream dos=new DataOutputStream(out);
			int n=count();
			byte[] b=null;
			try {
				dos.writeInt(n);
				boolean r=mylist.setBytes(dos);
				if (r)
					b=out.toByteArray();
			} catch (Exception e) {
				System.out.println("TextField1s.toBytes(write)::"+e.toString());
				b=null;
			}
			try {
				dos.close();
			} catch (Exception e) {
				System.out.println("TextField1s.toBytes(close)::"+e.toString());
			}
			out=null; dos=null;
			return b;

		} else {
			return null;
		}
	}

	//(内部クラス MyList) TextField1をリスト構造で保持する
	class MyList {
		public TextField1 textfield1=null;
		public MyList     next=null;
	
		//コンストラクタ
		MyList(TextField1 tf1, MyList nextmylist) {
			textfield1=tf1;
			next=nextmylist;
		}
		
		//リストのリセット
		public void reset() {
			if (next!=null)
				next.reset();
			next=null;
			textfield1=null;
		}
		
		//渡されたTextField1に該当するなら削除
		public MyList del(TextField1 tf1) {
			MyList ml;
			if (textfield1==tf1) {
				ml=next;
				tf1=null;
				next=null;
				return ml;
			} 
			if (next!=null) {
				next=next.del(tf1);
				return this;
			}
			return null;
		}
		
		//渡されたMyGoodsを持つTextField1ならそれを返す
		public TextField1 getTF1(MyGoods mg) {
			if (textfield1.mygoods==mg)
				return textfield1;
			if (next!=null)
				return next.getTF1(mg);
			return null;
		}
		
		//リストの該当番目ならTextField1を返す
		public TextField1 getCnt(int cnt) {
			if (cnt==0)
				return textfield1;
			if (next!=null)
				return next.getCnt(cnt-1);
			return null;
		}
		
		//リストの総数を返す
		public int count() {
			if (next!=null)
				return next.count()+1;
			return 1;
		}
		
		//渡されたTextField1を持っているなら番目を返す
		public int number(TextField1 tf1) {
			if (tf1==textfield1) {
				return 0;
			}
			if (next!=null)
				return next.number(tf1)+1;
			return -1;
		}
		
		//保存用byte配列にして返す
		public boolean setBytes(DataOutputStream dos) {
			boolean r=true;
			if (next!=null)
				r=next.setBytes(dos);
			if (r) {
				byte[] b=textfield1.mygoods.toBytes();
				if (b!=null) {
					int n=b.length;
					try {
						dos.writeInt(n);
						dos.write(b,0,n);
						return true;
					} catch (Exception e) {
						System.out.println("TextField1s.MyList.setBytes(write)::"+e.toString());
					}
				}
			}
			return false;
		}

	} // 内部クラスMyListの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //TextField1sの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
