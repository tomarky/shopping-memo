// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// TextField1.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class TextField1 extends TextField {
	public static final int CMD_CHANGE=10;
	public static final int CMD_DELETE=11;
	
	public MyGoods   mygoods=null;

	//コンストラクタ
	TextField1(ItemCommandListener icl, MyGoods mg) {
		super(mg.name,mg.details(),100,UNEDITABLE);
		
		setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		
		mygoods=mg;

		addCommand(new Command("変更",Command.SCREEN,CMD_CHANGE));
		addCommand(new Command("削除",Command.SCREEN,CMD_DELETE));
		setItemCommandListener(icl);
	}

	public void update() {
		setLabel(mygoods.name);
		setString(mygoods.details());
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //TextField1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
