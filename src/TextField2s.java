// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// TextField2s.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
import java.io.*;
//import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class TextField2s {

	private ItemCommandListener  icl=null;
	private MyList               mylist=null;

	//コンストラクタ
	TextField2s(ItemCommandListener c) {
		icl=c;
	}

	public void reset() {
		if (mylist!=null) {
			mylist.reset();
			mylist=null;
		}
	}

	//渡したTextField1を加える
	public void add(TextField2 tf2) {
		tf2.setItemCommandListener(icl);
		mylist=new MyList(tf2,mylist);
	}

	//渡したTextField2がリストに存在するなら削除する
	public void del(TextField2 tf2) {
		if (mylist!=null)
			mylist=mylist.del(tf2);
	}

	//渡したMyDataを持つTextField2があったら返す
	public TextField2 getTF1(MyData md) {
		if (mylist!=null)
			return mylist.getTF1(md);
		return null;
	}

	//リストのcnt番目TextField2を返す
	public TextField2 getCnt(int cnt) {
		if (mylist!=null)
			return mylist.getCnt(cnt);
		return null;
	}

	//リストに登録されてる総数を返す
	public int count() {
		if (mylist!=null)
			return mylist.count();
		return 0;
	}

	//渡したTextField2が何番目かを返す
	public int number(TextField2 tf2) {
		if (mylist!=null)
			return mylist.number(tf2);
		return -1;
	}

	//(内部クラス MyList) TextField2をリスト構造で保持する
	class MyList {
		public TextField2 textfield2=null;
		public MyList     next=null;
	
		//コンストラクタ
		MyList(TextField2 tf2, MyList nextmylist) {
			textfield2=tf2;
			next=nextmylist;
		}
		
		//リストのリセット
		public void reset() {
			if (next!=null)
				next.reset();
			next=null;
			textfield2=null;
		}
		
		//渡されたTextField2に該当するなら削除
		public MyList del(TextField2 tf2) {
			MyList ml;
			if (textfield2==tf2) {
				ml=next;
				tf2=null;
				next=null;
				return ml;
			} 
			if (next!=null) {
				next=next.del(tf2);
				return this;
			}
			return null;
		}
		
		//渡されたMyDataを持つTextField2ならそれを返す
		public TextField2 getTF1(MyData md) {
			if (textfield2.mydata==md)
				return textfield2;
			if (next!=null)
				return next.getTF1(md);
			return null;
		}
		
		//リストの該当番目ならTextField2を返す
		public TextField2 getCnt(int cnt) {
			if (cnt==0)
				return textfield2;
			if (next!=null)
				return next.getCnt(cnt-1);
			return null;
		}
		
		//リストの総数を返す
		public int count() {
			if (next!=null)
				return next.count()+1;
			return 1;
		}
		
		//渡されたTextField2を持っているなら番目を返す
		public int number(TextField2 tf2) {
			if (tf2==textfield2) {
				return 0;
			}
			if (next!=null)
				return next.number(tf2)+1;
			return -1;
		}
		
	} // 内部クラスMyListの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //TextField2sの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
