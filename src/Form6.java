// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form6.java  フォーム
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form6 extends Form {
	
	TextField1 textfield1;
	
	//コンストラクタ
	Form6(CommandListener cl, TextField1 tf11) {
		super("確認");
		
		textfield1=tf11;
		
		MyGoods mygoods=tf11.mygoods;
		
		TextField tf1;

		tf1=new TextField("商品名",mygoods.name,12,TextField.UNEDITABLE );
		tf1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		append(tf1);
		
		tf1=new TextField("値段",Long.toString(mygoods.price),10,TextField.UNEDITABLE );
		tf1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		append(tf1);

		tf1=new TextField("個数",Long.toString(mygoods.count),5,TextField.UNEDITABLE );
		tf1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		append(tf1);

		String  s;
		if (mygoods.intax)
			s="税込み価格";
		else
			s="税抜き価格";

		tf1=new TextField("消費税",s,s.length(),TextField.UNEDITABLE );
		tf1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		append(tf1);
		
		StringItem si1=new StringItem(null,"\n\n削除しますか？");
		si1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		append(si1);
	
		addCommand(new Command("はい"  ,Command.OK    ,0));
		addCommand(new Command("いいえ",Command.CANCEL,1));
		
		setCommandListener(cl);
	}
	
	public TextField1 data() {
		return textfield1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form6の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
