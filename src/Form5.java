// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form5.java  �t�H�[��
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// �C���|�[�g
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form5 extends Form implements ItemStateListener {

	TextField1s   tf1s=null;
	int           tax;

	ChoiceGroup   cg1=null;
	TextField     tf1=null;

	//�R���X�g���N�^
	Form5(CommandListener cl, TextField1s textfield1s, int t) {
		super("�v�Z");
		
		tf1s=textfield1s;
		tax=t;


		String[] s={"�ō��ݑ��z","�Ŕ������z","����ő��z"};
		
		cg1=new ChoiceGroup("�v�Z���@",Choice.EXCLUSIVE,s,null);
		cg1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);

		tf1=new TextField("���v���z",Long.toString(totalwithtax()),20,TextField.UNEDITABLE);
		tf1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		
		append(cg1);
		append(tf1);
		
		setItemStateListener(this);
		
		addCommand(new Command("�߂�",Command.OK,0));
		addCommand(new Command("��",Command.SCREEN,0));
		
		setCommandListener(cl);
	}

	private long totalwithtax() {
		int c=tf1s.count();
		int i;
		long t=0L;
		
		for (i=0;i<c;i++) {
			t+=tf1s.getCnt(i).mygoods.total(tax);
		}
		return t;
	}
	
	private long totalnotax() {
		int c=tf1s.count();
		int i;
		long t=0L;
		
		for (i=0;i<c;i++) {
			t+=tf1s.getCnt(i).mygoods.totalnotax(tax);
		}
		return t;
	}
	
	private long totaltax() {
		int c=tf1s.count();
		int i;
		long t=0L;
		
		MyGoods mg;
		for (i=0;i<c;i++) {
			//t+=tf1s.getCnt(i).mygoods.totaltax(tax);
			mg=tf1s.getCnt(i).mygoods;
			t+=mg.total(tax)-mg.totalnotax(tax);
		}
		return t;
	}
	
	public void itemStateChanged(Item item) {
		if (item==cg1) {
			switch (cg1.getSelectedIndex()) {
			case 0:
				tf1.setString(Long.toString(totalwithtax()));
				break;
			case 1:
				tf1.setString(Long.toString(totalnotax()));
				break;
			case 2:
				tf1.setString(Long.toString(totaltax()));
				break;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form5�̏I���

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
